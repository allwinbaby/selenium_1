#steps
#1 Open BT home Page
#2 Accept the cookies
#3 go to broadband offers page
#4 take screenshots and save

import time
from selenium import webdriver
from selenium.webdriver.common.by import By

#creating chrome driver instance
driver = webdriver.Chrome() # location of driver will be python scripts folder

# opening BT home page
driver.get("https://www.bt.com/")
driver.save_screenshot("Screenshots_1/home Page.png")

#Accept Cookies [Accept Button is present in an IFRAME which is of cross-broswer origin]
driver.switch_to.frame(driver.find_element(By.XPATH, "//iframe[contains(@class, 'trust')]")) #Switching to IFRAME

#wait for element
time.sleep(5)
driver.save_screenshot("Screenshots_1/Accept Cookies IFRAME.png")

#finding Accept button
accept_button = driver.find_element(By.XPATH, "//a[text()='Accept all cookies']") #Accept is in ankor element
accept_button.click() #clicking anker element

#Sitch to Main window
driver.switch_to.default_content()
time.sleep(5)

#finding the broad band offer page
broad_band_offers = driver.find_element(By.XPATH, "//a[@class='lc-Link lc-LinkBT lc-LinkBT--sizeINHERIT' and "
                                                  "@data-analytics-link='BT Product:: Your broadband offers']")
broad_band_offers.click()
time.sleep(5)
driver.save_screenshot("Screenshots_1/Broad Band Offers.png")

# Close the WebDriver
driver.quit()






'''  
#codes tried

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

#driver.find_element(By.XPATH, "/html/body/div[8]/div[1]/div/div[3]/a[1]").click() #finding accept cookies by FULL XPATH
driver.switch_to.frame(driver.find_element(By.XPATH, "//iframe[contains(@class, 'trust')]")) #Switching to iframe
accept = driver.find_element(By.XPATH, "//a[text()='Accept all cookies']") # #finding accept cookies 
driver.execute_script("document.querySelector('.call').click()") #to click anker tag which is dynamically generated 
try:
    accept = driver.find_element(By.XPATH, "//a[text()='Accept all cookies']")
except Exception as e:
    print(e)
else:
    print(accept)
    #//iframe[@class='truste_popframe']
# print(driver.page_source)
# driver.find_elements(By.XPATH, "//a[@class='call']")
'''
